extends Node2D

export var pointer_move_speed = 1.0

func _ready():
	yield(get_tree(), "idle_frame")
	update_pointer_position(0)

func update_pointer_position(objective_number):
	$MessageSound.play()
	
	var pointer = $ObjectivePointer
	var place = $ObjectivePositions.get_child(objective_number)
	var message = $ObjectiveMessages.get_child(objective_number)
	
	$TutorialGUI/Control/NinePatchRect/Label.text = message.message

	$Tween.interpolate_property(
		pointer, 
		"position", 
		pointer.position, 
		place.position, 
		pointer_move_speed, 
		Tween.TRANS_SINE, 
		Tween.EASE_IN_OUT)
		
	$Tween.start()
	$TutorialGUI/Control/AnimationPlayer.play("ChangeMessage")


func _on_MoveObjective_body_entered(body):
	update_pointer_position(1)


func _on_DoorObjective_body_entered(body):
	update_pointer_position(2)


func _on_NightVisionObjective_body_entered(body):
	update_pointer_position(3)
	get_tree().call_group("Interface", "DARK_mode")


func _on_BriefcaseObjective_body_entered(body):
	update_pointer_position(4)
