#!/bin/bash

#set -e
#set -x

# Set up some variables
EXPORT_NAME=$(basename `pwd`)
BUILD_DIR=build
PROJECTS=(
	"Linux/X11"
	"Windows Desktop"
	"Mac OSX"
)


# Clear and recreate our build directory
rm -rf ${BUILD_DIR}
mkdir -p ${BUILD_DIR}


# Build each output type
for i in "${PROJECTS[@]}"
do :
	echo "Building '${EXPORT_NAME}' for ${i} ..."
	godot -v --export ${i} "./${BUILD_DIR}/${EXPORT_NAME}"
done
