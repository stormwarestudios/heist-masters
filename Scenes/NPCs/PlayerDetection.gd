extends "res://Scenes/Characters/TemplateCharacter.gd"

const FOV_TOLERANCE = deg2rad(20.0)
const RED = Color(1, 0.1, 0.1)
const WHITE = Color(1, 1, 1)
const MAX_DETECTION_RANGE = 640

var Player


func _ready():
	Player = get_node("/root").find_node("Player", true, false)


func _process(delta):
	if Player_in_FOV() and Player_in_LOS():
		$Torch.color = RED
		get_tree().call_group("SuspicionMeter", "player_seen")
	else:
		$Torch.color = WHITE


func Player_in_FOV():
	var npc_facing_direction = Vector2(1, 0).rotated(global_rotation)
	var direction_to_Player = (Player.position - global_position).normalized()

	if abs(direction_to_Player.angle_to(npc_facing_direction)) < FOV_TOLERANCE:
		return true
	else:
		return false


func Player_in_LOS():
	var space = get_world_2d().direct_space_state
	var LOS_obstacle = space.intersect_ray(global_position, Player.global_position, [self], collision_mask)

	if not LOS_obstacle:
		return false

	var distance_to_player = Player.global_position.distance_to(global_position)
	if (distance_to_player >= MAX_DETECTION_RANGE):
		return false

	if LOS_obstacle.collider == Player:
		return true
	else:
		return false
