extends CanvasModulate

const DARK = Color("111111")
const NIGHTVISION = Color("37BF62")
var can_cycle = true


func _ready():
	visible = true
	color = DARK


func cycle_vision_mode():
	if can_cycle == false:
		return
		
	if color == NIGHTVISION:
		DARK_mode()
	elif color == DARK:
		NIGHTVISION_mode()
		
	can_cycle = false
	$Timer.start()


func DARK_mode():
	color = DARK
	$AudioStreamPlayer2D.stream = load("res://Assets/SFX/nightvision_off.wav")
	$AudioStreamPlayer2D.play()
	get_tree().call_group("labels", "hide")
	get_tree().call_group("lights", "show")
	
	
func NIGHTVISION_mode():
	color = NIGHTVISION
	$AudioStreamPlayer2D.stream = load("res://Assets/SFX/nightvision.wav")
	$AudioStreamPlayer2D.play()
	get_tree().call_group("labels", "show")
	get_tree().call_group("lights", "hide")
	

func _on_Timer_timeout():
	can_cycle = true
