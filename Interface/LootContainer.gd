extends NinePatchRect


func _ready():
	hide()


func collect_loot():
	if not visible:
		show()
	
	$VBoxContainer/ItemList.add_icon_item(load("res://Assets/GFX/Loot/suitcase.png"))
